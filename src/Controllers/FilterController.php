<?php


class FilterController
{
    public function getCurrentFilters(): Filters
    {
        return new Filters(
            categoryFilter: $_GET['category'],
            genderFilter: $_GET['gender'],
            birthDateFilter: $_GET['birthDate'],
            ageFilter: $_GET['age'],
            ageFrom: $_GET['ageFrom'],
            ageTo: $_GET['ageTo']
        );
    }

    public function show(): string
    {
        /** @var Filters $filters */
        $filters = $this->getCurrentFilters();
        return "<form method='get'>
    Категория: <input type='text' name='category' value='{$filters->categoryFilter}'>
    Пол: <input type='text' name='gender' value='{$filters->genderFilter}'>
    Дата рождения (YYYY-MM-DD): <input type='text' name='birthDate' value='{$filters->birthDateFilter}'>
    Возраст: <input type='text' name='age' value='{$filters->ageFilter}'>
    Возрастной диапазон: от <input type='number' name='ageFrom' value='{$filters->ageFrom}'> до <input type='number' name='ageTo' value='{$filters->ageTo}'>
    <input type='submit' value='Фильтровать'>
                </form>";
    }
}