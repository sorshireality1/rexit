<?php

final class AppController
{
    const ROWS_PER_PAGE = 25;

    /**
     * @var mixed|string
     */
    private mixed $inputFilename;
    /**
     * @var mixed|string
     */
    private mixed $outputFilename;


    private ReadContract $readController;
    private ExportContract $exportController;
    private FilterController $filtersController;

    public function __construct(
        $inputFilename = 'dataset.csv',
        $outputFilename = 'output.csv'
    )
    {
        $this->inputFilename = $inputFilename;
        $this->outputFilename = $outputFilename;
        $this->readController = new CSVReadController();
        $this->exportController = new CSVExportController();
        $this->filtersController = new FilterController();
    }

    public function show()
    {
        $this->exportController->show();
        $filtered = $this->readController->read(
            filename: $this->inputFilename,
            filters: $this->filtersController->getCurrentFilters()
        );
        $filteredData = $filtered['data'];


        echo "<table border='1'>";
        $currentPage = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $startRow = ($currentPage - 1) * self::ROWS_PER_PAGE;
        $totalRows = $filtered['count'];
        $endRow = min($startRow + self::ROWS_PER_PAGE, $totalRows);

// Отображаем данные с учетом пагинации
        for ($i = $startRow; $i < $endRow; $i++) {
            echo "<tr>";
            foreach ($filteredData[$i] as $cell) {
                echo "<td>" . htmlspecialchars($cell) . "</td>";
            }
            echo "</tr>";
        }

        echo "</table>";
        $totalRows = count($filteredData);
        $totalPages = ceil($totalRows / self::ROWS_PER_PAGE);
        $startRow = ($currentPage - 1) * self::ROWS_PER_PAGE;
        $endRow = min($startRow + self::ROWS_PER_PAGE, $totalRows);


//
    }
}