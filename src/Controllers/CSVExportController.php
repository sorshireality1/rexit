<?php


class CSVExportController implements ExportContract
{
    function export($data): void
    {
        // Создание CSV файла
        $csvFilename = 'exported_data.csv';
        $csvFile = fopen($csvFilename, 'w');

        foreach ($data as $rowData) {
            fputcsv($csvFile, $rowData);
        }

        fclose($csvFile);

        // Отправка файла пользователю
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . basename($csvFilename) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        readfile($csvFilename);

        exit;
    }

    function show(): string
    {
        return '<form method="post">
    <input type="hidden" name="export_data" value="1">
    <input type="submit" value="Экспорт в CSV">
                </form>';
    }
}