<?php

class CSVReadController implements ReadContract
{

    public function read(string $filename, Filters $filters): array
    {
        $file = new SplFileObject($filename);
        $filteredData = [];
        while (!$file->eof()) {
            $line = $file->fgets();
            $data = explode(",", $line);

            // Применяем фильтры
            if (($filters->categoryFilter && $data[0] != $filters->categoryFilter) ||
                ($filters->genderFilter && $data[4] != $filters->genderFilter) ||
                ($filters->birthDateFilter && $data[5] != $filters->birthDateFilter)) {
                continue;
            }

            // Фильтр по возрасту
            if ($filters->ageFilter || ($filters->ageFrom && $filters->ageTo)) {
                $age = date_diff(date_create($data[5]), date_create('today'))->y;
                if (($filters->ageFilter && $age != $filters->ageFilter) ||
                    ($filters->ageFrom && $age < $filters->ageFrom) ||
                    ($filters->ageTo && $age > $filters->ageTo)) {
                    continue;
                }
            }

            $filteredData[] = $data;
        }
        return [
            'data' => $filteredData,
            'count' => $file->key()
        ];
    }
}