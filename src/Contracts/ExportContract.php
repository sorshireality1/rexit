<?php

interface ExportContract
{
    public function export(array $data): void;
    public function show(): string;
}

?>