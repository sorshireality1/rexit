<?php

interface ReadContract
{
    public function read(string $filename, Filters $filters): array;
}