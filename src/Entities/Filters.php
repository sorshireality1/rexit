<?php

class Filters
{
    public $categoryFilter;
    public $genderFilter;
    public $birthDateFilter;
    public $ageFilter;
    public $ageFrom;
    public $ageTo;

    public function __construct(
        $categoryFilter = "",
        $genderFilter = "",
        $birthDateFilter = "",
        $ageFilter = "",
        $ageFrom = "",
        $ageTo = ""
    )
    {
        $this->categoryFilter = $categoryFilter;
        $this->genderFilter = $genderFilter;
        $this->birthDateFilter = $birthDateFilter;
        $this->ageFilter = $ageFilter;
        $this->ageFrom = $ageFrom;
        $this->ageTo = $ageTo;
    }
}

?>