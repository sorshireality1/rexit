<?php
require_once "Entities/Filters.php";
require_once "Contracts/ExportContract.php";
require_once "Contracts/ReadContract.php";
require_once "Controllers/AppController.php";
require_once "Controllers/CSVExportController.php";
require_once "Controllers/CSVReadController.php";
require_once "Controllers/FilterController.php";

$app = new AppController();
$app->show();
?>
