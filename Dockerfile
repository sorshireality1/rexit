FROM php:8.2-apache

# Copy your application's source code to the /var/www/html directory inside the container
COPY src/* /var/www/html

# Set the working directory to /var/www/html
WORKDIR /var/www/html

# Expose port 80 to access the Apache server
EXPOSE 80
